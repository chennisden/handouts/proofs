\documentclass{marticl}

\usepackage{amsmath}

\title{Proofs}
\author{Dennis Chen}

\begin{document}

\maketitle

\begin{abstract}
	People see proofs as these mystical things that they have no idea how to produce. True, a ``proof'' is more evocative than a mere ``explanation''. But an explanation does not need to cross the gates of Mordor before it is permitted to become a proof.

	Here, we dispel the notion that proofs are difficult to write and briefly explain how to write a good proof. Eventually, we touch on why it is not so important to be concerned about what precisely constitutes a proof.
\end{abstract}

\section{A story about cars}

Think back to when you were learning to drive. You get behind the wheel, drive up to a parking lot, and are about to do your first left turn. So you ask your mom and pop, ``How far do I need to turn the steering wheel?'' And perhaps the technically correct answer is ``It depends on how fast you're going, but typically around $110^{\circ}$.'' But you're a human and $110^{\circ}$ doesn't mean anything to you, so they tell you to feel it out.

Mathematical proofs are the same. There are as many conventions to follow in mathematics as there are rules in driving, but everyone has a good feel for the road. Don't think too hard about proofs. Just turn.

\section{What's a proof?}

\begin{defi}[The Thesis]
	A proof is a
	\begin{enumerate}
		\item correct,
		\item convincing,
		\item and sequential
	\end{enumerate}
	mathematical argument for how, given a set of initial assumptions, a desired conclusion may be derived.
\end{defi}

Let's start with what ``initial assumptions'' means. Some of them are implicit, and others are explicit.

\begin{exam}
	Suppose that $x$, $y$, and $z$ are positive reals. Show that $x+y+z$ is positive.
\end{exam}

Our explicit assumptions are easy: $x$, $y$, and $z$ are positive reals. It's what the problem says!

But what are our implicit assumptions? It turns out that they will always be exactly what we need to prove the theorem, no more and no less. Typically they are quite fundamental facts or results that can be (re)derived easily, where ``fundamental'' and ``easily'' are relative to the theorem at hand. Or they are universal assumptions (which we call \emph{axioms}) that hold semi-consistently throughout mathematics.

In this example, one of the things we know about $\mathbb{R}$ is that $\mathbb{R}$ is an ordered field. The relevant implication is that given two positive reals $x$ and $y$, $x+y$ is also a positive real.

Implicit assumptions can be a lot less trivial. You may need to recall some substantive lemma, or even assume that you may use some technique such as induction.\footnote{The validity of induction is given by the Axiom of Induction in the Peano axioms. It is not necessarily inherent to all forms of mathematical logic, so it is an assumption, albeit one we almost always make.}

\begin{pro}
	Note that $x+y$ is a positive real as $x$ and $y$ are. Thus $(x+y)+z$ is too, as desired.
\end{pro}

Okay, that's a sequential and correct argument. It's sequential because we took the hypotheses, stepped through an intermediate conclusion, and used the intermediate conclusion to derive our final conclusion. And it's correct because every step was justified with one of the assumptions we started out with. But what makes a proof convincing?

\section{Convince by being neat}

The best way to make your proofs more convincing is to think more clearly. Here's an example:

\begin{exam}
	If $k$ is an integer, show that $k$ divides $2k$.
\end{exam}

\begin{pro}(Bad)
	Clearly $2k$ is a multiple of $k$. Isn't this obviously true?
\end{pro}

On the surface, the issue is that this proof isn't correct: there's no real justification for anything, anywhere. But look closer: what are the steps in this argument? What idea is it trying to convey? You don't know, and odds are, neither does the author.

It's true that a proof like this is unfixable. But that doesn't mean you have to undergo some magical transformation to start writing good proofs. In this example, all the author needed to do was have a clear idea what the word ``divisible'' means. (Here, the clear idea is just the definition. Sometimes it is more involved.)

\begin{defi}[Divisibility]
	An integer $a$ divides an integer $b$ if and only if there exists some integer $c$ such that $ac = b$.
\end{defi}

\begin{pro}(Good)
	There exists an integer $c$ such that $kc = 2k$: that integer is $c=2$. Thus, by the definition of divisibility, $k$ divides $2k$.
\end{pro}

\section{But don't sweat the details}

In the last example, it made sense to explicitly cite the definition of divisibility. That's because the whole point of the proof was citing this definition. But in a more involved problem it would not.

The purpose of a proof is to get to the heart of the matter. Sometimes (in fact usually), it is okay to expand our initial assumptions beyond just the axioms of mathematics.

When we write more complex proofs about divisibility, we trust that everyone watching at home can show that $k$ divides $2k$ on their own. So we omit these verifications, because they'd be a trivial distraction to the real manipulations we are doing.

But sometimes we'd like to take for granted a fact that is difficult to prove in its own right. No problem: all we have to do is lay the groundwork. First prove Lemma A separately, and then use it to prove Theorem B.

A big part of being convincing is focusing on the big steps you make. Sometimes this means splitting the entire proof into a few lemmas. But also, this usually this means omitting some details.

In some sense, you aren't really writing formal proofs most of the time because you aren't axiomatically justifying every step you take. That's okay. The real goal of a proof is to communicate a mathematical idea, not to manipulate a string of symbols according to formally defined rules. So don't sweat the details too much.

\section{On style}

Being able to write correct, convincing, and sequential proofs is still not enough. There's one final ingredient: style.

Typically, ``having good style'' just means following conventions. The way you pick up conventions is pretty simple: just read other proofs. This is not something you should go out of your way to do: by learning and doing real mathematics, you will naturally be exposed to many proofs to theorems and solutions to exercises.

Supposing you have a decent grasp of what the conventions of mathematical proofs are, how do you abide by them? By instinctually noticing when something \emph{feels} wrong and then having the compulsion to fix it. By setting high standards for yourself, you'll ensure that even if you're struggling to write a good proof, at least by the end you won't have written a bad one. More on this \href{https://dennisc.net/writing/essays/standards}{here}.

Some conventions I want to emphasize:

\begin{enumerate}
	\item Never start a sentence with math. All sentences must start with English. (Reason: it just looks wrong.)

		Some people are more strict and instead mandate that no \emph{punctuation} (including periods) may be followed with English. I do not believe this is necessary.

	\item Use ``we'' instead of ``I''. Instead of ``I want to show that $4$ is even'', ``we want to show that $4$ is even''.

	\item If a problem says ``show for all $x$ that $P(x)$ is true'', do not say ``Fix a value of $x$ and then do [series of manipulations]''. Instead, say ``For any $x$, [series of manipulations]''. (Reason: the manipulations you are doing work for any $x$, so you do not need to take the extra step of fixing some value of $x$.)

	\item \href{https://dennisc.net/writing/essays/specific}{Use the most specific word possible}. For example, instead of saying ``simplify $x^2 + xy$ into $x(x+y)$'', say ``factor $x^2 + xy$ into $x(x+y)$''. (Reason: you get extra clarity without losing any conciseness.)

	\item Instead of saying ``$Y$ if $X$'', say ``if $X$ then $Y$''. (Reason: we are used to dealing with a structure of hypotheses on the left, conclusion on the right. For instance, we write $X\implies Y$.)

		This is more personal taste and is not necessarily standard.
\end{enumerate}

One more thing --- and this is logical, not stylistic --- know the difference between ``if'' and ``if and only if''. A brief study of propositional logic will not hurt.

When you are writing something by hand (usually in an exam) the standards are looser. However, in all other cases, you \emph{MUST} typeset your solutions with LaTeX, which you \emph{SHOULD} install locally (e.g. TeX Live). Refer to \href{https://web.evanchen.cc/latex-style-guide.html}{Evan Chen's style guide} and \href{https://web.evanchen.cc/handouts/LaTeXPetPeeve/LaTeXPetPeeve.pdf}{pet peeves} for typesetting guidelines.

\end{document}
